﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class box : MonoBehaviour
{
    public lvlSys lvlsys;
    public deliveryLvl delivery;

    void Start()
    {
        lvlsys = GameObject.FindGameObjectWithTag("table").GetComponent<lvlSys>();
    }

    void Update()
    {
        if (delivery.start == true)
        {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Rigidbody>().useGravity = true;
            transform.parent = null;
        }
        if (transform.position.y <= -10f)
        {
            Destroy(this.gameObject);
        }
    }
}
