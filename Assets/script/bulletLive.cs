﻿using UnityEngine;

public class bulletLive : MonoBehaviour
{
    private float t;

    private void Start() => t = 3f;
    
    private void Update()
    {
        t -= Time.deltaTime;
        if (t <= 0) Destroy(this.gameObject);
    }
}
