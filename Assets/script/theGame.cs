﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class theGame : MonoBehaviour
{
    private bool inGame = false;
    public UI ui;
    public GameObject textTablic, cartechObj, cartechObj1, cartechObj2, cartechObj3, cartechObj4, cartechObj5, cartechObj6, cartechObj7, cartechObj8, cartechObj9;
    public gun gun;
    public lvlSys levelSys;

    private void Update()
    {
        if (inGame == true)
        {
            textTablic.GetComponent<TextMeshPro>().text = "Осталось " + gun.shot.ToString();
            if (gun.shot >= 10)
            {
                cartechObj.SetActive(true);
                cartechObj1.SetActive(true);
                cartechObj2.SetActive(true);
                cartechObj3.SetActive(true);
                cartechObj4.SetActive(true);
                cartechObj5.SetActive(true);
                cartechObj6.SetActive(true);
                cartechObj7.SetActive(true);
                cartechObj8.SetActive(true);
                cartechObj9.SetActive(true);
            }
            else
            {
                switch (gun.shot)
                {
                    case 9:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(true);
                        cartechObj4.SetActive(true);
                        cartechObj5.SetActive(true);
                        cartechObj6.SetActive(true);
                        cartechObj7.SetActive(true);
                        cartechObj8.SetActive(true);
                        cartechObj9.SetActive(false);
                        break;
                    case 8:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(true);
                        cartechObj4.SetActive(true);
                        cartechObj5.SetActive(true);
                        cartechObj6.SetActive(true);
                        cartechObj7.SetActive(true);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 7:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(true);
                        cartechObj4.SetActive(true);
                        cartechObj5.SetActive(true);
                        cartechObj6.SetActive(true);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 6:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(true);
                        cartechObj4.SetActive(true);
                        cartechObj5.SetActive(true);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 5:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(true);
                        cartechObj4.SetActive(true);
                        cartechObj5.SetActive(false);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 4:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(true);
                        cartechObj4.SetActive(false);
                        cartechObj5.SetActive(false);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 3:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(true);
                        cartechObj3.SetActive(false);
                        cartechObj4.SetActive(false);
                        cartechObj5.SetActive(false);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 2:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(true);
                        cartechObj2.SetActive(false);
                        cartechObj3.SetActive(false);
                        cartechObj4.SetActive(false);
                        cartechObj5.SetActive(false);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    case 1:
                        cartechObj.SetActive(true);
                        cartechObj1.SetActive(false);
                        cartechObj2.SetActive(false);
                        cartechObj3.SetActive(false);
                        cartechObj4.SetActive(false);
                        cartechObj5.SetActive(false);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                    default:
                        cartechObj.SetActive(false);
                        cartechObj1.SetActive(false);
                        cartechObj2.SetActive(false);
                        cartechObj3.SetActive(false);
                        cartechObj4.SetActive(false);
                        cartechObj5.SetActive(false);
                        cartechObj6.SetActive(false);
                        cartechObj7.SetActive(false);
                        cartechObj8.SetActive(false);
                        cartechObj9.SetActive(false);
                        break;
                }
            }
            if (levelSys.lotBox <= 0 && levelSys.levelNow<=11)
            {
                levelSys.levelNow++;
                levelSys.loadLevel(levelSys.levelNow);
                fb.Instance.LevelEnded(levelSys.levelNow);
                gun.ready = false;
                gun.shot = 3 * levelSys.levelNow;
            }
        }
    }
    public void startGame()
    {
        inGame = true;
        ui.levelPanel.SetActive(true);
    }
    public void restart()
    {
        levelSys.restart();
    }
}
