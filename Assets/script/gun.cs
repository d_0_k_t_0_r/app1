﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun : MonoBehaviour
{
    public GameObject muzzle, cam, gunObj, platgormForGun, core;
    private float thrust = 1500f;
    public int shot=10;
    public bool ready;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (shot > 0 && ready==true)
            {
                Vector3 point = Input.mousePosition;
                Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(point);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    platgormForGun.transform.LookAt(new Vector3(hit.point.x, 0f, 18.25f));
                    gunObj.transform.LookAt(hit.point);

                    GameObject newCore = Instantiate(core, muzzle.transform.position, gunObj.transform.rotation);
                    newCore.transform.parent = null;
                    newCore.GetComponent<Rigidbody>().AddForce(newCore.transform.forward * thrust, ForceMode.Force);
                }
                shot--;
            }
        }
    }
}
