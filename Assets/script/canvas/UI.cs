﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    public lvlSys levelSystem;
    public gun gunSystem;
    public GameObject restart, start, levelProgress,restartGame,levelPanel;
    public Text levelNow, levelNext;
    public Image finish;

    private void Update()
    {
        levelNow.text = levelSystem.levelNow.ToString();
        if (levelSystem.levelNow + 1 == 11) 
        { 
            levelNext.enabled = false;
            finish.enabled = true;
        }
        else
        {
            levelNext.enabled = true;
            finish.enabled = false;
        }
        levelNext.text = (levelSystem.levelNow + 1).ToString();
        levelProgress.GetComponent<Slider>().maxValue = levelSystem.allBoxInLevel;
        levelProgress.GetComponent<Slider>().value = (levelSystem.allBoxInLevel - levelSystem.lotBox);

        if (gunSystem.shot == 0)
        {
            restart.SetActive(true);
        }
        else
        {
            restart.SetActive(false);
        }
        if (levelSystem.levelNow == 11)
        {
            levelPanel.SetActive(false);
            restartGame.SetActive(true);
        }
    }
    public void resGame()
    {
        SceneManager.LoadScene("game");
    }
}
