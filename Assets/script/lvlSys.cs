﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class lvlSys : MonoBehaviour
{
    private GameObject[] box;
    private GameObject deliveryLVL;
    private bool generateLvl, boxInScene;
    public GameObject[] level;
    public int lotBox, levelNow,allBoxInLevel;
    public gun gunSys;
    private void Start()
    {
        levelNow = 0;
    }
    private void Update()
    {
        if (boxInScene == true)
        {
            box = GameObject.FindGameObjectsWithTag("target");
            lotBox = box.Length;
            if (lotBox <= 0) boxInScene = false;
        }
        if (generateLvl == true)
        {
            gunSys.ready = deliveryLVL.GetComponent<deliveryLvl>().start;
            if (gunSys.ready == true) generateLvl = false;
        }
    }

    public void loadLevel(int numberLvl)
    {
        if (levelNow <= 11)
        {
            deliveryLVL = Instantiate(level[numberLvl - 1], new Vector3(-0.04f, 7f, 8.61f), Quaternion.identity);
            allBoxInLevel = deliveryLVL.GetComponent<deliveryLvl>().allBox;
            generateLvl = true;
            boxInScene = true;
        }
    }

    public void restart()
    {
        gunSys.ready = false;
        gunSys.shot = 3 * levelNow;
        int temp = lotBox - 1;
        for(int i = temp; i > -1; i--)
        {
            Destroy(box[i]);
        }
        box = new GameObject[0];
        loadLevel(levelNow);
    }
}
