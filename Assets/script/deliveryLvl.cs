﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deliveryLvl : MonoBehaviour
{
    private float t;
    private Vector3 up, down;

    public bool liftUp;
    public bool start;
    public int allBox;

    private void Start()
    {
        start = false;
        up = transform.position;
        down = new Vector3(-0.04f, 0.05f, 8.61f);
        liftUp = true;
    }
    private void Update()
    {
        transform.position = Vector3.Lerp(up, down, t);
        if (liftUp == true)
        {
            t += Time.deltaTime;
            if (t >= 1)
            {
                start = true;
                liftUp = false;
            }
        }
        else
        {
            t -= Time.deltaTime;
            if (t <= 0) Destroy(this.gameObject);
        }
    }
}
